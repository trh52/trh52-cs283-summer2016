
//Standard library includes
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include <assert.h>
#include <string.h>

//These defines are needed to unlock all the features I need in ftw.h
#define _XOPEN_SOURCE 500
#define __USE_XOPEN_EXTENDED 1
#define __USE_GNU 1

//Syscall libraries
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <ftw.h>

//Personal header for EZ malloc'ing
#include "../util.h"

//Returns true if the file exists, false otherwise
uint8_t exists(const char* filename){
  return access(filename, F_OK) == 0;
}

//Copies source to dest, returns 0 on success
int copy(const char* source, const char* dest){
  printf("Copying \"%s\" to \"%s\"...", source, dest);
  FILE *f_src, *f_dest;
  const static size_t bufsize = 512;
  char buf[bufsize];
  int retval = 0;
  if((f_src = fopen(source, "r")) != NULL){
    if((f_dest = fopen(dest, "w")) != NULL){
      while(fgets(buf, bufsize, f_src) != NULL){
	if(fprintf(f_dest, "%s", buf) < 0){
	  perror("Error copying file");
	  retval = 3;
	  break;
	}
      }
      fclose(f_dest);
      printf("DONE\n");
    }else{
      printf("ERROR\n");
      perror("Could not open dest file");
      retval = 2;
    }
    fclose(f_src);
  }else{
    printf("ERROR\n");
    perror("Could not open source file");
    retval = 1;
  }
  return retval;
}

//This is the callback function for nftw for deleting directories recursively
int delete_cb(const char *fpath, const struct stat *sb,
	      int typeflag, struct FTW *ftwbuf){
  printf("Deleting: \"%s\"...", fpath);
  if(remove(fpath) != 0){
    printf("ERROR\n");
    perror("Error deleting file");
  }else{
    printf("DONE\n");
  }
  return FTW_CONTINUE;
}

//Works like "rm -r" returns whatever nftw does
int rmdir_r(const char* path){
  printf("Deleting %s recursively...\n", path);
  //FTW_DEPTH makes it go into post-order so it will call on the deepest file first
  //This makes it perfect since it will only reach a directory after deleting
  //all its contents which makes remove happy
  return nftw(path, delete_cb, 10, FTW_DEPTH | FTW_ACTIONRETVAL);
}

//Converts the filepath to start from dirpath
//  convert("baz", "foo/bar") => "baz/bar"
//malloc's the result, so remember to free it!
char* convert(const char* dir_path, const char* file_path){
  const char* start = file_path;
  while(*start != 0 && *start != '/'){
    start++;
  }
  assert(*start == '/');
  ++start;
  size_t len = strlen(dir_path)+strlen(start)+1;
  char* res = NEW_ARRAY(char, len+1);
  sprintf(res, "%s/%s", dir_path, start);
  return res;
}

//Globals I need for the process callbacks
char *dir_a_g, *dir_b_g;

//Callback for processing the A directory
int process_a_cb(const char *fpath, const struct stat *sb,
		 int typeflag, struct FTW *ftwbuf){
  printf("Processing: %s", fpath);
  if(strcmp(fpath, dir_a_g) == 0){
    printf("\n");//This is the root, do nothing
    return FTW_CONTINUE;
  }
  char* counterpart = convert(dir_b_g, fpath);
  printf("\tcp: %s\n", counterpart);
  struct stat cp_stat;
  if(exists(counterpart)){//This file exists in b
    if(typeflag == FTW_F){//This is a regular file
      stat(counterpart, &cp_stat);
      time_t my_time = sb->st_mtime, their_time = cp_stat.st_mtime;
      double diff = difftime(my_time, their_time);
      //printf("my_time=%f\ttheir_time=%f\tdiff=%f\n",
      //     (double)my_time,(double)their_time, diff);
      if(diff < 0){ //Mine is newer
	copy(counterpart, fpath);
      }else{ //Mine is older
	copy(fpath, counterpart);
      }
    }
    //Do nothing for directories
  }else{//This file does not exist in b
    if(typeflag == FTW_F){//Copy the file
      copy(fpath, counterpart);
    }else if(typeflag == FTW_D){//Make the directory
      printf("Creating directory: %s\tmode=%4o\n", counterpart, sb->st_mode);
      if(mkdir(counterpart, sb->st_mode) != 0){
	perror("Error making directory");
      }
    }
  }
  free(counterpart);
  return FTW_CONTINUE;
}

//Callback for processing the B directory
int process_b_cb(const char *fpath, const struct stat *sb,
		 int typeflag, struct FTW *ftwbuf){
  printf("Processing: %s", fpath);
  if(strcmp(fpath, dir_b_g) == 0){
    printf("\n");//This is the root, do nothing
    return FTW_CONTINUE;
  }
  char* counterpart = convert(dir_a_g, fpath);
  printf("\tcp: %s\n", counterpart);
  if(!exists(counterpart)){//This file does not exist in A
    if(typeflag == FTW_F){//This is a file, delete it
      //Call the callback since it has all those nice messages and stuff
      delete_cb(fpath, sb, typeflag, ftwbuf);
    }else if(typeflag == FTW_D){//This is a directory, delete it recursively
      rmdir_r(fpath);
    }
  }
  free(counterpart);
  return FTW_CONTINUE;
}

//Where the magic happens! Just calls nftw on the callbacks
int process(char* dir_a, char* dir_b){
  dir_a_g = dir_a;
  dir_b_g = dir_b;
  printf("PROCESSING DIRECTORY A\n");
  /*int res =*/ nftw(dir_a, process_a_cb, 10, FTW_ACTIONRETVAL);
  //if(res != FTW_STOP) return -1;
  printf("PROCESSING DIRECTORY B\n");
  /*res =*/ nftw(dir_b, process_b_cb, 10, FTW_ACTIONRETVAL);
  //if(res != FTW_STOP) return -1;
  printf("COMPLETE!\n");
  return 0;
}

int main(int argc, char** argv){
  if(argc < 3){
    printf("Using syncro:\n\tsyncro <DIR A> <DIR B>\n");
    return -1;
  }
  setvbuf(stdout, NULL, _IONBF, 0);
  char *dir_a = argv[1], *dir_b = argv[2];
  return process(dir_a, dir_b);
}
