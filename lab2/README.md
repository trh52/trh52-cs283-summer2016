Lab 2 - File Manipulation
======
By: Tobias Highfill

Hey, what's up? I decided to use ftw.h because all the file opening and looping and error checking was giving me a headache, so why not let some library do all the heavy lifting for me? The only downside was I needed global variables to remember the name of the other directory for comparing. That's no big deal, unless I was going to multithread this code (god forbid, I don't want to even think about the race conditions!).

I have commented the code fairly thoroughly, so take a look if you want.
