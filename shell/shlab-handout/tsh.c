/* 
 * tsh - A tiny shell program with job control
 * 
 * Tobias Highfill (trh52)
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>

/* Misc manifest constants */
#define MAXLINE    1024   /* max line size */
#define MAXARGS     128   /* max args on a command line */
#define MAXJOBS      16   /* max jobs at any point in time */
#define MAXJID  (1<<16)   /* max job ID */

/* Job states */
#define UNDEF 0 /* undefined */
#define FG 1    /* running in foreground */
#define BG 2    /* running in background */
#define ST 3    /* stopped */

#ifdef DEBUG_MODE
//Print stuff only when compiled in debug mode
#define DEBUG(...) printf(__VA_ARGS__)
#else //Not in debug mode, do nothing
#define DEBUG(...) 
#endif //defined? DEBUG_MODE

//Passes args to syscall and checks for an error
#define CHECK_ERR(syscall,...) {if(syscall(__VA_ARGS__) != 0)unix_error(#syscall);}

/* 
 * Jobs states: FG (foreground), BG (background), ST (stopped)
 * Job state transitions and enabling actions:
 *     FG -> ST  : ctrl-z
 *     ST -> FG  : fg command
 *     ST -> BG  : bg command
 *     BG -> FG  : fg command
 * At most 1 job can be in the FG state.
 */

/* Global variables */
extern char **environ;      /* defined in libc */
char prompt[] = "tsh> ";    /* command line prompt (DO NOT CHANGE) */
int verbose = 0;            /* if true, print additional output */
int nextjid = 1;            /* next job ID to allocate */
char sbuf[MAXLINE];         /* for composing sprintf messages */

typedef struct job_t {              /* The job struct */
    pid_t pid;              /* job PID */
    int jid;                /* job ID [1, 2, ...] */
    int state;              /* UNDEF, BG, FG, or ST */
    char cmdline[MAXLINE];  /* command line */
} job_t;
struct job_t jobs[MAXJOBS]; /* The job list */

/*
typedef struct str_queue str_queue;
struct str_queue{
  char* str;
  str_queue* next;
  str_queue* prev;
};
str_queue pqueue;
*/
/* End global variables */


/* Function prototypes */

/* Here are the functions that you will implement */
int isAllWhitespace(const char* str);
pid_t execute(char** argv, int inputFD, int outputFD, int closeThis);
void eval(char *cmdline);
int builtin_cmd(char **argv);
void do_bgfg(char **argv);
void waitfg();
void waitfgpid(pid_t pid);

//int checkKill(pid_t pid);
void printJobSignal(const char* msg, job_t* job, int sig);
void printStopMsg(job_t* job, int sig);
void printInterruptMsg(job_t* job, int sig);
void reapChildren();
void killpid_group(pid_t pid, int sig);
void sigchld_handler(int sig);
void sigtstp_handler(int sig);
void sigint_handler(int sig);

/* Here are helper routines that we've provided for you */
int parseline(const char *cmdline, char **argv); 
void sigquit_handler(int sig);

void clearjob(struct job_t *job);
void initjobs(struct job_t *jobs);
int maxjid(struct job_t *jobs); 
int addjob(struct job_t *jobs, pid_t pid, int state, char *cmdline);
void deletejob(struct job_t* jobs, struct job_t* remove);
int deletejobpid(struct job_t *jobs, pid_t pid); 
pid_t fgpid(struct job_t *jobs);
struct job_t *getjobpid(struct job_t *jobs, pid_t pid);
struct job_t *getjobjid(struct job_t *jobs, int jid); 
int pid2jid(pid_t pid); 
int printjob(struct job_t* job);
void listjobs(struct job_t *jobs);

void usage(void);
void unix_error(char *msg);
void app_error(char *msg);
typedef void handler_t(int);
handler_t *Signal(int signum, handler_t *handler);

/*
 * main - The shell's main routine 
 */
int main(int argc, char **argv) 
{
    char c;
    char cmdline[MAXLINE];
    int emit_prompt = 1; /* emit prompt (default) */

    /* Redirect stderr to stdout (so that driver will get all output
     * on the pipe connected to stdout) */
    dup2(1, 2);

    /* Parse the command line */
    while ((c = getopt(argc, argv, "hvp")) != EOF) {
        switch (c) {
        case 'h':             /* print help message */
            usage();
	    break;
        case 'v':             /* emit additional diagnostic info */
            verbose = 1;
	    break;
        case 'p':             /* don't print a prompt */
            emit_prompt = 0;  /* handy for automatic testing */
	    break;
	default:
            usage();
	}
    }

    /* Install the signal handlers */

    /* These are the ones you will need to implement */
    Signal(SIGINT,  sigint_handler);   /* ctrl-c */
    Signal(SIGTSTP, sigtstp_handler);  /* ctrl-z */
    Signal(SIGCHLD, sigchld_handler);  /* Terminated or stopped child */

    /* This one provides a clean way to kill the shell */
    Signal(SIGQUIT, sigquit_handler); 

    /* Initialize the job list */
    initjobs(jobs);

    /* Execute the shell's read/eval loop */
    while (1) {

	/* Read command line */
	if (emit_prompt) {
	    printf("%s", prompt);
	    fflush(stdout);
	}
	if ((fgets(cmdline, MAXLINE, stdin) == NULL) && ferror(stdin))
	    app_error("fgets error");
	if (feof(stdin)) { /* End of file (ctrl-d) */
	    fflush(stdout);
	    exit(0);
	}

	/* Evaluate the command line */
	eval(cmdline);
	fflush(stdout);
	fflush(stdout);
    } 

    exit(0); /* control never reaches here */
}

//Checks if a string is only comprised of whitespace characters or is empty
int isAllWhitespace(const char* str){
  for(int i=0; str[i] != 0; ++i){
    switch(str[i]){
    case ' ': case '\t': case '\r': case '\n':
      break;
    default:
      return 0;
    }
  }
  return 1;
}

/* 
 * execute - fork() and exec a new process. "closeThis" will be closed
 *           in the child process if it is positive, otherwise it is ignored.
 */
pid_t execute(char** argv, int inputFD, int outputFD, int closeThis){
  pid_t pid;
  switch((pid = fork())){
  case -1: //Failure!!
    printf("Failed to fork()!\n");
    unix_error("fork");
  case 0: //This is the child process
    //Creates its own process group so I can
    //send signals to it and all its children
    DEBUG("Changing PGID\n");
    if(setpgid(0,0) == -1)
      unix_error("setpgid");
    //in/out redirection
    if(closeThis >= 0){//Some weird pipe glitch shit
      close(closeThis);
    }
    DEBUG("Redirecting input\n");
    if(dup2(inputFD, fileno(stdin)) == -1)
      unix_error("input redirect");
    DEBUG("Redirecting output\n");
    if(dup2(outputFD, fileno(stdout)) == -1)
      unix_error("output redirect");
    //Execute other process
    execvp(argv[0], argv);
    if(errno == ENOENT){
      printf("%s: Command not found\n", argv[0]);
      exit(1);
    }else{
      unix_error("execvp");
    }
  default: //This is the parent
    break;
  }
  DEBUG("Executed process: (%d) %s\n", pid, argv[0]);
  return pid;
}
  
/* 
 * eval - Evaluate the command line that the user has just typed in
 * 
 * If the user has requested a built-in command (quit, jobs, bg or fg)
 * then execute it immediately. Otherwise, fork a child process and
 * run the job in the context of the child. If the job is running in
 * the foreground, wait for it to terminate and then return.  Note:
 * each child process must have a unique process group ID so that our
 * background children don't receive SIGINT (SIGTSTP) from the kernel
 * when we type ctrl-c (ctrl-z) at the keyboard.  
*/
void eval(char *cmdline) {
  if(isAllWhitespace(cmdline)){
    return;//This used to cause segfaults!
  }
  char* argv[50]; //should be big enough
  char **pipeArgs = NULL; //Args to pass to pipe process
  int isBG = parseline(cmdline, argv);
  FILE *inFile = NULL, *outFile = NULL;
  //Check the argslist for file redirection and pipes
  for(int i=0; argv[i]!=NULL; ++i){
    if(strcmp(argv[i], ">") == 0
       || strcmp(argv[i], "<") == 0
       || strcmp(argv[i], ">>") == 0){
      //File redirection
      //increment i so we skip over the filename next iteration
      char *type = argv[i], *fname = argv[++i];
      //Set the spot that had the symbols to NULL to stop the args list there
      argv[i-1] = NULL; 
      //These are user errors so we just return to give them another chance
      if(fname == NULL){
	printf("Syntax error! expected filename after '%s'\n", type);
	return;
      }
      if(type[0] == '>'){ //Output redirection
	//If the length is two we know it must be >> so append
	outFile = fopen(fname, strlen(type) == 2 ? "a" : "w");
	if(outFile == NULL){
	  perror("Could not open output");
	  return;
	}
      }else{ //Input redirection
	inFile = fopen(fname, "r");
	if(inFile == NULL){
	  perror("Could not open input");
	  return;
	}
      }
    }else if(strcmp(argv[i], "|") == 0){
      //Pipes!
      if(pipeArgs != NULL){ //We already have a pipe!
	printf("Sorry! Currently tsh only supports one pipe!\n");
	return;
      }
      if(argv[i+1] == NULL){ //Check if there's something after the pipe
	printf("Syntax error: expected a command after '|'\n");
	return;
      }
      //Set this spot to null to end the args list for the first command
      argv[i] = NULL;
      ++i;//The next token is our command so skip that
      pipeArgs = argv+i;//Set pipeArgs to start with that command
    }
  }
  if(builtin_cmd(argv) == 0){ //Not a builtin
    //spawn child process(es)
    pid_t base, piped;
    int state = isBG? BG : FG;
    //FDs for in/out
    int baseInput = fileno(inFile == NULL? stdin : inFile);
    int baseOutput = fileno(outFile == NULL? stdout : outFile);
    if(pipeArgs == NULL){
      //No piping, simply execute
      base = execute(argv, baseInput, baseOutput, -1);
    }else{
      //Piping!
      int pipeFDs[2] = {0,0};
      if(pipe(pipeFDs) != 0){
	unix_error("pipe construction");
      }
      base = execute(argv, baseInput, pipeFDs[1], pipeFDs[0]);
      piped = execute(pipeArgs, pipeFDs[0], baseOutput, pipeFDs[1]);
      addjob(jobs, piped, state, cmdline);
      //Close the pipes in this process. Otherwise it hangs!
      CHECK_ERR(close, pipeFDs[0]);
      CHECK_ERR(close, pipeFDs[1]);
    }
    addjob(jobs, base, state, cmdline);
    if(isBG){//Print out status for user
      printf("[%d] (%d) %s", pid2jid(base), base, cmdline);
    }else{
      waitfg(); //wait for fg processes
    }
  }
  DEBUG("End of eval(\"%s\")\n", cmdline);
}

/* 
 * parseline - Parse the command line and build the argv array.
 * 
 * Characters enclosed in single quotes are treated as a single
 * argument.  Return true if the user has requested a BG job, false if
 * the user has requested a FG job.  
 */
int parseline(const char *cmdline, char **argv) 
{
    static char array[MAXLINE]; /* holds local copy of command line */
    char *buf = array;          /* ptr that traverses command line */
    char *delim;                /* points to first space delimiter */
    int argc;                   /* number of args */
    int bg;                     /* background job? */

    strcpy(buf, cmdline);
    buf[strlen(buf)-1] = ' ';  /* replace trailing '\n' with space */
    while (*buf && (*buf == ' ')) /* ignore leading spaces */
	buf++;

    /* Build the argv list */
    argc = 0;
    if (*buf == '\'') {
	buf++;
	delim = strchr(buf, '\'');
    }
    else {
	delim = strchr(buf, ' ');
    }

    while (delim) {
	argv[argc++] = buf;
	*delim = '\0';
	buf = delim + 1;
	while (*buf && (*buf == ' ')) /* ignore spaces */
	       buf++;

	if (*buf == '\'') {
	    buf++;
	    delim = strchr(buf, '\'');
	}
	else {
	    delim = strchr(buf, ' ');
	}
    }
    argv[argc] = NULL;
    
    if (argc == 0)  /* ignore blank line */
	return 1;

    /* should the job run in the background? */
    if ((bg = (*argv[argc-1] == '&')) != 0) {
	argv[--argc] = NULL;
    }
    return bg;
}

/* 
 * builtin_cmd - If the user has typed a built-in command then execute
 *    it immediately.  
 */
int builtin_cmd(char **argv){
  if(strcmp(argv[0], "quit") == 0){
    exit(0);
  }else if(strcmp(argv[0], "fg") == 0
	   || strcmp(argv[0], "bg") == 0){
    do_bgfg(argv);
  }else if(strcmp(argv[0], "jobs") == 0){
    listjobs(jobs);
  }else{
    return 0;     /* not a builtin command */
  }
  return 1;
}

/* 
 * do_bgfg - Execute the builtin bg and fg commands
 */
void do_bgfg(char **argv) 
{
  job_t* target;
  //Error checking
  if(argv[1] == NULL){ // need an argument
    printf("%s command requires PID or %%jobid argument\n", argv[0]);
    return;
  }
  //It's a JID if it starts with %
  int val, isjid=(argv[1][0] == '%');
  int ret = sscanf(argv[1], isjid? "%%%d" : "%d", &val);
  if(ret == EOF || ret < 1){//Parse error
    printf("%s: argument must be a PID or %%jobid\n", argv[0]);
    return;
  }
  if(isjid){
    target = getjobjid(jobs, val);
    if(target==NULL){ //Not found
      printf("%%%d: No such job\n", val);
      return;
    }
  }else{
    target = getjobpid(jobs, val);
    if(target == NULL){ //Not found
      printf("(%d): No such process\n", val);
      return;
    }
  }
  //send SIGCONT to the process
  killpid_group(target->pid, SIGCONT);
  //Update the state
  if(argv[0][0] == 'f'){
    target->state = FG;
  }else{
    target->state = BG;
    printf("[%d] (%d) %s", target->jid, target->pid, target->cmdline);
  }
}

//Wait for all foreground jobs to complete
//This is necessary for piping since both are FG and they could end at any time
void waitfg(){
  pid_t pid = fgpid(jobs);
  while(pid != 0){
    waitfgpid(pid);
    pid = fgpid(jobs);
  }
}

/* 
 * waitfg - Block until process pid is no longer the foreground process
 */
void waitfgpid(pid_t pid) {
  if(pid == 0) return;
  DEBUG("Waiting for pid %d\n", pid);
  while(fgpid(jobs) == pid){ //Empty loop check
    //reapChildren();
  }
  DEBUG("Wait for pid %d is over\n", pid);
}

/*
int checkKill(pid_t pid){
  return kill(pid, 0) != 0 && errno == ESRCH;
}
*/

//These next functions are just for convenience

void printJobSignal(const char* msg, job_t* job, int sig){
  printf("Job [%d] (%d) %s by signal %d\n", job->jid, job->pid, msg, sig);
}

void printStopMsg(job_t* job, int sig){
  printJobSignal("stopped", job, sig);
}

void printInterruptMsg(job_t* job, int sig){
  printJobSignal("terminated", job, sig);
}

//Loops through all jobs and checks if they're dead or stopped
//Removes dead jobs and prints signal messages for the user
void reapChildren(){
  int killcount = 0;
  for(int i=0; i < MAXJOBS; ++i){
    pid_t pid = jobs[i].pid;
    if(pid > 0){
      //if(isDead(pid)){
      //Get info about process
      int wstatus, tmp = waitpid(pid, &wstatus, WNOHANG | WUNTRACED);
      if(tmp > 0){
	if(WIFEXITED(wstatus) || WIFSIGNALED(wstatus)){
	  //Process is dead
	  DEBUG("Found dead process %d, deleting.\n", pid);
	  if(WIFSIGNALED(wstatus)){
	    //Process was termianted via signal
	    printInterruptMsg(jobs+i, WTERMSIG(wstatus));
	  }
	  deletejob(jobs, jobs+i);
	  ++killcount;
	}else if(WIFSTOPPED(wstatus)){
	  //Process was stopped
	  printStopMsg(jobs+i, WSTOPSIG(wstatus));
	  jobs[i].state = ST;
	}
      }else if(tmp < 0){//error
	unix_error("waitpid");
      }
      //}
    }
  }
  if(killcount>0)
    DEBUG("Deleted %d jobs\n", killcount);
}

/*****************
 * Signal handlers
 *****************/

/* 
 * sigchld_handler - The kernel sends a SIGCHLD to the shell whenever
 *     a child job terminates (becomes a zombie), or stops because it
 *     received a SIGSTOP or SIGTSTP signal. The handler reaps all
 *     available zombie children, but doesn't wait for any other
 *     currently running children to terminate.  
 */
void sigchld_handler(int sig) 
{
  DEBUG("Caught SIGCHLD\n");
  reapChildren();
}

//Sends a signal to every process in pid's group
void killpid_group(pid_t pid, int sig){
  pid_t pgid = getpgid(pid);//get PGID
  if(pgid < 0){
    unix_error("getpgid");
    return;
  }
  if(kill(-pgid,sig) != 0)
    unix_error("kill");
}

/* 
 * sigint_handler - The kernel sends a SIGINT to the shell whenver the
 *    user types ctrl-c at the keyboard.  Catch it and send it along
 *    to the foreground job.  
 */
void sigint_handler(int sig) {
  DEBUG("Caught SIGINT\n");
  pid_t fgJob=fgpid(jobs);
  if(fgJob!=0){
    killpid_group(fgJob, sig);
    //job_t* job = getjobpid(jobs, fgJob);
    //printInterruptMsg(job, sig);
  }
}

/*
 * sigtstp_handler - The kernel sends a SIGTSTP to the shell whenever
 *     the user types ctrl-z at the keyboard. Catch it and suspend the
 *     foreground job by sending it a SIGTSTP.  
 */
void sigtstp_handler(int sig) {
  DEBUG("Caught SIGTSTP\n");
  pid_t fgJob=fgpid(jobs);
  if(fgJob!=0){
    killpid_group(fgJob,sig);
    job_t* job = getjobpid(jobs, fgJob);
    job->state = ST;//Update state
    //printStopMsg(job, sig);
  }
}

/*********************
 * End signal handlers
 *********************/

/***********************************************
 * Helper routines that manipulate the job list
 **********************************************/

/* clearjob - Clear the entries in a job struct */
void clearjob(struct job_t *job) {
    job->pid = 0;
    job->jid = 0;
    job->state = UNDEF;
    job->cmdline[0] = '\0';
}

/* initjobs - Initialize the job list */
void initjobs(struct job_t *jobs) {
    int i;

    for (i = 0; i < MAXJOBS; i++)
	clearjob(&jobs[i]);
}

/* maxjid - Returns largest allocated job ID */
int maxjid(struct job_t *jobs) 
{
    int i, max=0;

    for (i = 0; i < MAXJOBS; i++)
	if (jobs[i].jid > max)
	    max = jobs[i].jid;
    return max;
}

/* addjob - Add a job to the job list */
int addjob(struct job_t *jobs, pid_t pid, int state, char *cmdline) 
{
    int i;
    
    if (pid < 1)
	return 0;

    for (i = 0; i < MAXJOBS; i++) {
	if (jobs[i].pid == 0) {
	    jobs[i].pid = pid;
	    jobs[i].state = state;
	    jobs[i].jid = nextjid++;
	    if (nextjid > MAXJOBS)
		nextjid = 1;
	    strcpy(jobs[i].cmdline, cmdline);
  	    if(verbose){
	        printf("Added job [%d] %d %s\n", jobs[i].jid, jobs[i].pid, jobs[i].cmdline);
            }
            return 1;
	}
    }
    printf("Tried to create too many jobs\n");
    return 0;
}

//Delete a specific job from the list
void deletejob(struct job_t* jobs, struct job_t* remove){
  clearjob(remove);
  nextjid = maxjid(jobs)+1;
}

/* deletejob - Delete a job whose PID=pid from the job list */
int deletejobpid(struct job_t *jobs, pid_t pid) 
{
  int i;

  if (pid < 1)
    return 0;

  for (i = 0; i < MAXJOBS; i++) {
    if (jobs[i].pid == pid) {
      deletejob(jobs, jobs+i);
      return 1;
    }
  }
  return 0;
}

/* fgpid - Return PID of current foreground job, 0 if no such job */
pid_t fgpid(struct job_t *jobs) {
    int i;

    for (i = 0; i < MAXJOBS; i++)
	if (jobs[i].state == FG)
	    return jobs[i].pid;
    return 0;
}

/* getjobpid  - Find a job (by PID) on the job list */
struct job_t *getjobpid(struct job_t *jobs, pid_t pid) {
    int i;

    if (pid < 1)
	return NULL;
    for (i = 0; i < MAXJOBS; i++)
	if (jobs[i].pid == pid)
	    return &jobs[i];
    return NULL;
}

/* getjobjid  - Find a job (by JID) on the job list */
struct job_t *getjobjid(struct job_t *jobs, int jid) 
{
    int i;

    if (jid < 1)
	return NULL;
    for (i = 0; i < MAXJOBS; i++)
	if (jobs[i].jid == jid)
	    return &jobs[i];
    return NULL;
}

/* pid2jid - Map process ID to job ID */
int pid2jid(pid_t pid) 
{
    int i;

    if (pid < 1)
	return 0;
    for (i = 0; i < MAXJOBS; i++)
	if (jobs[i].pid == pid) {
            return jobs[i].jid;
        }
    return 0;
}

//Prints out a job's current state and info
int printjob(job_t* job){
  if(job == NULL){
    DEBUG("NULL job!\n");
    return 0;
  }
  printf("[%d] (%d) ", job->jid, job->pid);
  switch (job->state) {
  case BG: 
    printf("Running ");
    break;
  case FG: 
    printf("Foreground ");
    break;
  case ST: 
    printf("Stopped ");
    break;
  default:
    return 0;
  }
  printf("%s", job->cmdline);
  return 1;
}

/* listjobs - Print the job list */
void listjobs(struct job_t *jobs) {
  int i;
    
  for (i = 0; i < MAXJOBS; i++) {
    if (jobs[i].pid != 0) {
      if(printjob(jobs+i) == 0){
	printf("listjobs: Internal error: job[%d].state=%d ", 
	       i, jobs[i].state);
      }
    }
  }
}
/******************************
 * end job list helper routines
 ******************************/


/***********************
 * Other helper routines
 ***********************/

/*
 * usage - print a help message
 */
void usage(void) 
{
    printf("Usage: shell [-hvp]\n");
    printf("   -h   print this message\n");
    printf("   -v   print additional diagnostic information\n");
    printf("   -p   do not emit a command prompt\n");
    exit(1);
}

/*
 * unix_error - unix-style error routine
 */
void unix_error(char *msg)
{
    fprintf(stdout, "%s: %s\n", msg, strerror(errno));
    exit(1);
}

/*
 * app_error - application-style error routine
 */
void app_error(char *msg)
{
    fprintf(stdout, "%s\n", msg);
    exit(1);
}

/*
 * Signal - wrapper for the sigaction function
 */
handler_t *Signal(int signum, handler_t *handler) 
{
    struct sigaction action, old_action;

    action.sa_handler = handler;  
    sigemptyset(&action.sa_mask); /* block sigs of type being handled */
    action.sa_flags = SA_RESTART; /* restart syscalls if possible */

    if (sigaction(signum, &action, &old_action) < 0)
	unix_error("Signal error");
    return (old_action.sa_handler);
}

/*
 * sigquit_handler - The driver program can gracefully terminate the
 *    child shell by sending it a SIGQUIT signal.
 */
void sigquit_handler(int sig) 
{
    printf("Terminating after receipt of SIGQUIT signal\n");
    exit(1);
}



