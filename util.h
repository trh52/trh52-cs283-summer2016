
#ifndef __UTIL_H__
#define __UTIL_H__

#include <stdlib.h>

#define NEW_ARRAY(type, len) ((type*) malloc(sizeof(type) * (len)))
#define NEW_POINTER(type) NEW_ARRAY(type,1)
#define CHECK_NULL(val) {if((val)==NULL){printf("Malloc failed! line: %d file: %s\n", __LINE__, __FILE__); exit(-1);}}


#endif //__UTIL_H__
