/*
  Problem 1
  Define an int* pointer variable, and create an array of 10 integers using malloc().
  Then, assign values to that array, print their values, and free() the integers.
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include "../util.h"

int main(void){
  int32_t* arr = NEW_ARRAY(int32_t, 10);
  CHECK_NULL(arr);
  for(int32_t i=0; i<10; i++){
    arr[i] = (i+5)*2;
  }
  for(int32_t i=0; i<10; i++){
    printf("arr[%d] = %d\n", i, arr[i]);
  }
  free(arr);
  printf("Memory freed!\n");
}
