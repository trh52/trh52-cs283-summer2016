

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "../util.h"

#define FAST_VERSION

#ifdef FAST_VERSION
#define RESIZE(old) ((old)*2)
#else
#define RESIZE(old) ((old)+1)
#endif

typedef struct ArrayList{
  int32_t* data;
  int32_t length;
  size_t size;
} ArrayList;

void init_list(ArrayList* dest, size_t size){
  dest->size = size;
  dest->length = 0;
  dest->data = NEW_ARRAY(int32_t, size);
}

int32_t add(ArrayList* list, int32_t val){
  if(list == NULL) return 0;
  if(list->length >= list->size){
    size_t newsize = RESIZE(list->size);
    while(newsize <= list->length){
      newsize = RESIZE(newsize);
    }
    //printf("Reallocating to size %d\n", newsize);
    int32_t* newdata = (int32_t*) realloc((void*)list->data, newsize * sizeof(int32_t));
    if(newdata == NULL){
      free(list->data);
      printf("Error reallocating memory\n");
      list->data = NEW_ARRAY(int32_t, newsize);
    }else{
      list->data = newdata;
    }
    list->size = newsize;
  }
  list->data[list->length] = val;
  ++(list->length);
  return 1;
}

int32_t list_remove(ArrayList* list, int32_t idx){
  if(list == NULL || idx >= list->length) return 0;
  for(int32_t i = idx; i<list->length-1; ++i){
    list->data[i] = list->data[i+1];
  }
  --(list->length);
  return 1;
}

int32_t get(ArrayList* list, int32_t idx, int32_t* dest){
  if(list == NULL || idx >= list->length) return 0;
  *dest = list->data[idx];
  return 1;
}

int main(){
  int32_t initial = 4, final = 1000000;
  ArrayList list;
  init_list(&list, initial);
  for(int32_t i=0; i<final; ++i){
    add(&list, i);
    printf("Added %d\r", i);
  }
  printf("\nDone!\n");
  free(list.data);
}
