CS 283 Lab 1
======
By Tobias Highfill

##Compiling and Running
```
  $ make all
  $ build/prb1
  $ build/prb2
  $ build/prb3_4
  $ build/prb5
```

To change problem 5's resizing algorithm you'll need to edit prb5.c and comment out the line

```C
#define FAST_VERSION
```

and recompile.

##Note on problem 4

Problem 4 is included alongside problem 3 but due to what I can only assume is **HEINOUS BLACK MAGIC** it does not work at all despite all laws of logic and programming I know of. Take a look at the output if you don't believe me. The location of pivot is NEVER changed in the code yet somehow it's different each iteration and NOT EVEN AN ASSERT CAN CATCH IT, WTF?! the swap_nodes function only works SOMETIMES. This code is evil, be warned.
