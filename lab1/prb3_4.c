/*
  Problem 3
  Write a function sort() that takes in an int* a and int size,
  and sorts the array using pointer arithmetic.
*/

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <assert.h>

#include "../util.h"

#define SWAP(type, v1, v2) {type tmp=v1;v1=v2;v2=tmp;}

//Quicksort in place
void sort(int32_t* arr, int32_t len){
  if(len <= 1){
    return;
  }
  int32_t pivot = 0;
  int32_t pval = arr[pivot];
  for(int32_t i=1; i<len; ++i){
    if(arr[i] < pval){
      SWAP(int32_t, arr[pivot], arr[pivot+1]);
      if(i != pivot+1){
	SWAP(int32_t, arr[pivot], arr[i]);
      }
      ++pivot;
    }
  }
  sort(arr, pivot);
  if(pivot<len-1)
    sort(arr+pivot+1, len-pivot-1);
}

typedef struct ListNode ListNode;
struct ListNode{
  int32_t val;
  ListNode *prev, *next;
};

void delete(ListNode* list){
  while(list!=NULL){
    ListNode* tmp = list->next;
    free(list);
    list = tmp;
  }
}

ListNode* push_front(ListNode* head, int32_t val){
  ListNode* res = NEW_POINTER(ListNode);
  res->val = val;
  res->prev = NULL;
  res->next = head;
  if(head != NULL) head->prev = res;
  return res;
}

void print_node(ListNode* node){
  if(node == NULL){
    printf("NULL node\n");
  }else{
    printf("%p -> {val:%d, prev:%p, next:%p}\n", node, node->val, node->prev, node->next);
  }
}

int32_t swap_nodes(ListNode* node1, ListNode* node2){
  printf("swap_nodes(%p, %p) Before:\n", node1, node2);
  print_node(node1);
  print_node(node2);
  if(node1 == NULL || node2 == NULL){
    return 0;
  }
  if(node1 == node2) return 1;
  ListNode *n1prev = node1->prev;
  ListNode *n1next = node1->next;
  ListNode *n2prev = node2->prev;
  ListNode *n2next = node2->next;
  if(n1prev != NULL) n1prev->next = node2;
  if(n1next != NULL) n1next->prev = node2;
  if(n2prev != NULL) n2prev->next = node1;
  if(n2next != NULL) n2next->prev = node1;
  node1->prev = n2prev;
  node1->next = n2next;
  node2->prev = n1prev;
  node2->next = n1next;
  printf("After:\n");
  print_node(node1);
  print_node(node2);
  return 1;
}

ListNode* find_head(ListNode* node){
  if(node == NULL) return NULL;
  ListNode* res = node;
  while(res->prev != NULL){
    res = res->prev;
  }
  return res;
}

ListNode* sort_list(ListNode* head, const ListNode* end){
  if(head == end || head->next == end) return head;
  ListNode* pivot = head;
  const ListNode* pivot_c = pivot;
  const int32_t pval = pivot->val;
  ListNode* node = pivot->next;
  while(node!=end){
    assert(pivot == pivot_c);
    if(node->val < pval){
      ListNode* adjacent = pivot->next, *nxttmp = node->next;
      printf("pivot:%p, adjacent:%p, node:%p\n", pivot, adjacent, node);
      assert(adjacent != NULL);
      assert(swap_nodes(pivot, adjacent) != 0);
      assert(swap_nodes(adjacent, node) != 0);
      node = adjacent;
      assert(node->next == nxttmp);
    }
    node = node->next;
    assert(pivot == pivot_c);
  }
  head = sort_list(head, pivot);
  sort_list(pivot->next, end);
  return head;
}

int main(void){
  srand(time(NULL));
  int32_t len = 100;
  int32_t arr[len];
  ListNode* list = NULL;
  printf("Unsorted:\n");
  for(int32_t i = 0; i< len; ++i){
    arr[i] = rand()%1000;
    list = push_front(list, arr[i]);
    printf("arr[%d] = %d\n", i, arr[i]);
  }
  printf("Sorting array...");
  sort(arr, len);
  printf("done!\nSorted:\n");
  for(int32_t i = 0; i< len; ++i){
    printf("arr[%d] = %d\n", i, arr[i]);
  }
  //printf("Sorting linkedlist...");
  ListNode* curr = list = sort_list(list, NULL);
  printf("done!\nTesting:\n");
  for(int32_t i = 0; i< len; ++i){
    assert(curr->val == arr[i]);
    curr = curr->next;
  }
  delete(list);
}
