/*
  Problem 2
  Using malloc, create a char** pointer that contains 10 char*'s, then in a loop,
  initialize each of the 10 char*'s in a loop to a char array of size 15, and
  initialize each to a word of your choice (don't forget the null terminator \0)
  -- and print them to screen.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../util.h"

int main(void){
  char** strings = NEW_ARRAY(char*, 10);
  CHECK_NULL(strings);
  for(int i=0; i<10; i++){
    strings[i] = NEW_ARRAY(char, 15);
    CHECK_NULL(strings[i]);
    strcpy(strings[i], "phlegathon");
    printf("strings[%d] = %s\n", i, strings[i]);
    free(strings[i]);
  }
  free(strings);
  return 0;
}
