

O = build

CC=  gcc

CFLAGS=-g -Wall -std=c99

$(O)/%.o: %.c $(O)
	  $(CC) $(CFLAGS) -c $< -o $@

$(O):
	mkdir $(O)
