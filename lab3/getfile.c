
#define __USE_POSIX 1
#define __USE_GNU 1

#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "csapp.h"
#include "../util.h"


int connectAndRead(char* server, int port, char* fpath){
  int senderFD;
  senderFD = open_clientfd(server, port);
  if(senderFD < 0){
    fprintf(stderr, "Could not create sender!\n");
    perror("sender");
    return -1;
  }
  printf("Got senderFD: %d\n", senderFD);
  FILE *fsender = fdopen(senderFD, "w+");
  fprintf(fsender, "GET %s HTTP/1.1\r\nHost: %s\r\n\r\n", fpath, server);
  if(fflush(fsender) != 0){
    perror("Could not flush");
    return -1;
  }
  printf("Request sent\nWaiting...\n\n");
  char buf[MAXLINE];
  while(fgets(buf, MAXLINE, fsender) != NULL){
    printf("%s", buf);
  }
  fclose(fsender);
  return 0;
}

void printUsage(){
  printf("getfile USAGE:\n\tgetfile [-h | --help] [-p <port>] <host> [<filepath>]\n");
}

int main(int argc, char** argv){
int port = 80, pflag=0, i;
  char *server = NULL, *fpath = NULL;
  if(argc == 1){
    printUsage();
    return -1;
  }
  for(i=1; i<argc; i++){
    if(pflag == 1){
      port = atoi(argv[i]);
      pflag = 0;
    }else if(strcmp(argv[i], "-p") == 0){
      pflag = 1;
    }else if(strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0){
      printUsage();
      return 0;
    }else if(server == NULL){
      server = argv[i];
    }else if(fpath == NULL){
      fpath = argv[i];
    }else{
      fprintf(stderr, "Warning: unprocessed argument \"%s\"\n", argv[i]);
    }
  }
  if(server == NULL){
    fprintf(stderr, "Error: No host provided!\n");
    return -1;
  }
  if(fpath == NULL){
    fpath = "/index.html";
  }
  printf("Host:\t%s\nPort:\t%d\nFile:\t%s\n", server, port, fpath);
  return connectAndRead(server, port, fpath);
}
