CS 283 - Lab 3 - Socket Programming
=====

By: Tobias Highfill (trh52)

##Building instructions

```
make all
```

##Running the Server

The first commandline argument is the port to use and the second optional argument is the directory to use as "root" for incoming requests (defaults to current working directory).

Example:
```
./server 1337 mysite
```

##Running the Client

Usage:
```
getfile [-h | --help] [-p <port>] <host> [<filepath>]
```

##Example

```
./server 1337 &
./getfile -p 1337 localhost csapp.h
```

For the host name you can also use the tux name like "tux64-13" if the server is running on a different node.