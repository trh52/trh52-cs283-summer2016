
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>

#include "csapp.h"
#include "../util.h"

char* processPath(char* root, char* path){
  int depth = 0;
  int p_idx = 1;
  for(p_idx = 0; path[p_idx] != 0; p_idx++){
    char* stub = path+p_idx;
    if(strncmp(stub, "/~/", 3) == 0
       || strncmp(stub, "//", 2) == 0){
      return NULL;
    }
    if(strncmp(stub, "/../", 4) == 0 && (--depth) < 0){
      return NULL;
    }
  }
  path++;
  char* res = NEW_ARRAY(char, strlen(root)+strlen(path)+2);
  sprintf(res, "%s/%s", root, path);
  return res;
}

int pipeStream(FILE* src, FILE* dest){
  char buf[MAXLINE];
  while(fgets(buf, MAXLINE, src) != NULL){
    if(fprintf(dest, "%s", buf) < 0){
      printf("Error sending data!\n");
      perror("send");
    }
    if(fflush(dest) == EOF)
      perror("flush");
  }
  if(feof(src)==0){
    perror("pipe");
    return -1;
  }
  return 0;
}

int main(int argc, char** argv){
  int port = 80;
  char* root = ".";
  if(argc < 2){
    printf("USAGE:\n\tserver <port> [<root>]\n");
    return -1;
  }
  port = atoi(argv[1]);
  if(argc >= 3) root = argv[2];
  printf("Port: %d\tRoot: %s\n", port, root);
  int sockfd = open_listenfd(port);
  while(1){
    printf("Listening...\n");
    int fd = accept(sockfd, NULL, NULL);
    if(fd < 0){
      printf("Unable to accept socket\n");
      perror("accept:");
      continue;
    }
    printf("Connection made!\n");
    FILE* in = fdopen(fd, "r");
    FILE* out = fdopen(dup(fd), "w");
    char buf[MAXLINE];
    fscanf(in, "GET %s HTTP/1.1\r\n", buf);
    char* path = processPath(root, buf);
    if(path == NULL){
      printf("User fucked with shit that should not be fucked with\n");
      fprintf(out, "HTTP/1.1 403 Forbidden\r\n\r\n");
    }else{
      printf("Path: %s\n", path);
      FILE* file = fopen(path, "r");
      if(file == NULL){
	fprintf(out, "HTTP/1.1 500 Internal Service Error\r\n");
	printf("Error: could not open %s\n", path);
	perror("open");
      }else{
	printf("Sending response...\n");
	fprintf(out, "HTTP/1.1 200 OK\r\nConnection: close\r\n\r\n");
	pipeStream(file, out);
	printf("Done!\n");
      }
    }
    fclose(in);
    fclose(out);
    printf("Connection closed\n");
  }
}
