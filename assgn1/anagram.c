
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <ctype.h>
#include <string.h>
#include <assert.h>

#include "../util.h"

#define CHAR_COUNT 26

typedef struct HashNode HashNode;
typedef struct ListNode ListNode;
struct HashNode{
  uint32_t hashval;
  HashNode* less;
  HashNode* more;
  ListNode* first;
};

struct ListNode{
  char* word;
  uint32_t counts[CHAR_COUNT];
  ListNode* next;
};

uint32_t hash(const char* word){
  uint32_t res = 0;
  for(uint32_t i = 0; word[i] != 0; ++i){
    char c = toupper(word[i]);
    if(c >= 'A' && c<= 'Z'){
      c -= 'A' + 1;
      res += c;
    }
  }
  return res;
}

void make_char_counter(uint32_t arr[], const char* word){
  for(int i=0; i<CHAR_COUNT; ++i){
    arr[i] = 0;
  }
  for(int i=0; word[i]!=0; ++i){
    char c = toupper(word[i]);
    if(c < 'A' || c > 'Z'){
      //printf("Anomalous char '%c' in '%s' at %d\n", c, word, i);
      continue;
    }
    ++(arr[c-'A']);
  }
}

ListNode* make_list_node(char* word, ListNode* next){
  ListNode* res = NEW_POINTER(ListNode);
  if(res == NULL){
    return NULL;
  }
  res->word = word;
  res->next = next;
  make_char_counter(res->counts, word);
  return res;
}

HashNode* make_hash_node(char* word, uint32_t hashval){
  HashNode* res = NEW_POINTER(HashNode);
  if(res == NULL){
    return NULL;
  }
  res->hashval = hashval;
  res->less = res->more = NULL;
  res->first = make_list_node(word, NULL);
  if(res->first == NULL){
    free(res);
    return NULL;
  }
  return res;
}

ListNode* insert_word_list(ListNode* list, char* word, int* success){
  *success = 1;
  if(list == NULL){
    return make_list_node(word, NULL);
  }
  int cmp = strcmp(word, list->word);
  if(cmp < 0){//Insert here
    ListNode* new = make_list_node(word, list);
    if(new == NULL){
      *success = 0;
      return list;
    }else{
      return new;
    }
  }else if(cmp > 0){//Move on
    list->next = insert_word_list(list->next, word, success);
  }
  return list;
}

int insert_word3(HashNode* table, char* word, uint32_t hashval){
  if(table == NULL || word == NULL){
    return 0;
  }
  HashNode** next;
  if(table->hashval == hashval){
    int succ;
    ListNode* head = insert_word_list(table->first, word, &succ);
    if(succ){
      table->first = head;
    }
    return succ;
  }else if(hashval < table->hashval){
    next = &(table->less);
  }else{
    next = &(table->more);
  }
  if(*next == NULL){
    *next = make_hash_node(word, hashval);
    return *next != NULL;
  }else{
    return insert_word3(*next, word, hashval);
  }
}

int insert_word(HashNode* table, char* word){
  return insert_word3(table, word, hash(word));
}

int compare(uint32_t arr1[], uint32_t arr2[], uint32_t length){
  for(uint32_t i=0; i<length; i++){
    if(arr1[i] < arr2[i]){
      return -1;
    }else if(arr1[i] > arr2[i]){
      return 1;
    }
  }
  return 0;
}

ListNode* lookup_list(HashNode* table, char* word){
  uint32_t hashval = hash(word);
  if(table == NULL){
    printf("Table empty!\n");
    return NULL;
  }
  while(table != NULL && table->hashval != hashval){
    table = (hashval<table->hashval)?table->less:table->more;
  }
  if(table == NULL){
    printf("Unable to find word '%s' in hashtable!\n", word);
    return NULL;
  }
  return table->first;
}

void lookup_anagrams(HashNode* table, char* word, char letter, int32_t place){
  uint32_t counter[CHAR_COUNT];
  make_char_counter(counter, word);
  ListNode* list = lookup_list(table,word);
  uint32_t count = 0;
  while(list != NULL){
    if(strcmp(word, list->word) != 0
       && compare(counter, list->counts, CHAR_COUNT) == 0
       && (place < 0 || list->word[place] == letter)){
      printf("%s\n", list->word);
      ++count;
    }
    list = list->next;
  }
  if(count == 0){
    printf("Sorry! I got nothing!\n");
  }else{
    printf("Found %d results!\n", count);
  }
}

void delete_list(ListNode* list){
  ListNode* next;
  while(list != NULL){
    free(list->word);
    next = list->next;
    free(list);
    list = next;
  }
}

void delete_table(HashNode* table){
  if(table != NULL){
    delete_list(table->first);
    delete_table(table->less);
    delete_table(table->more);
    free(table);
  }
}

HashNode* load_table(){
  HashNode* table = NULL;
  FILE* dict = fopen("/usr/share/dict/words", "r"); //open the dictionary for read-only access
  if(dict == NULL){
    perror("Error loading the dictionary");
    return NULL;
  }
  printf("Loading dictionary, please wait...\n");
  char buffer[128];
  while(fgets(buffer, sizeof(buffer), dict) != NULL) {
    int len = strlen(buffer);
    if(buffer[len-1] == '\n'){
      buffer[len-1] = 0;
    }
    char* word = NEW_ARRAY(char, len);
    strcpy(word, buffer);
    if(table == NULL){
      table = make_hash_node(word, hash(word));
    }else{
      if(!insert_word(table, word)){
	printf("Unable to add '%s' to table!\n", word);
	delete_table(table);
	table = NULL;
	goto cleanup;
      }
    }
  }
  printf("Dictionary loaded!\n");
 cleanup:
  fclose(dict);
  return table;
}

void print_usage(){
  printf("Analook Usage:\n\tanalook <WORD> [<LETTER> <PLACE>]\n");
  printf("\nCompiled: %s, %s\n", __DATE__, __TIME__);
}

int main(int argc, char** argv){
  if(argc <= 1){
    print_usage();
    return 1;
  }
  char* my_word = argv[1];
  int32_t place = -1;
  char letter;
  if(argc == 4){
    letter = argv[2][0];
    sscanf(argv[3], "%d", &place);
  }else if(argc != 2){
    print_usage();
    return 1;
  }
  //Get down to business
  HashNode* table = load_table();
  if(table == NULL){
    return 2;
  }
  lookup_anagrams(table, my_word, letter, place);
  delete_table(table);
  printf("Data cleared!\n");
  return 0;
}
