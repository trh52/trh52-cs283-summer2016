Assignment 1 - Anagram Solver
======
By: Tobias Highfill

##Building and Running

```
  $ #Lookup anagrams
  $ WORD=race make anagram
  $ #Lookup anagrams with a character in a specific place
  $ WORD=race LETTER=c PLACE=1 make anagram
```

Running valgrind

```
  $ #Lookup anagrams
  $ WORD=race make anagram-d
  $ #Lookup anagrams with a character in a specific place
  $ WORD=race LETTER=c PLACE=1 make anagram-d
```

##Discussion question

A better implementation would be a traditional open table because then the bucket lookups will be constant time.