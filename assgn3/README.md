CS 283 - Assignment 3 - RSA Chat Server
=====

By: Tobias Highfill

#Compiling

This code requires the MissingH package (I was too lazy to write my own strip function). To install it run:

```
make install
```

or by hand:

```
cabal update
cabal install MissingH
```

Then use make to compile

#Running it

To start a server run ./chat with no arguments. To connect to a server as a client pass the hostname as the first and only argument, all others will be ignored.

In either mode the first thing you will do is enter two positive integers one at a time. Then the client will be able to send messages to the server and any other clients on that same server. The server cannot send messages. To quit the client hit Ctrl-C or type ":quit" and press enter. To quit the server press Ctrl-C.

#Cross-compatibility

My partner for this assignment was Paul Flack (phf29). We tested our programs for cross-compatibility so go ahead and test them together if you want.