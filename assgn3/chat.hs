
import RSA
import Data.ByteString (ByteString)
import qualified  Data.ByteString.Char8 as BC
import Network.Socket hiding (send, sendTo, recv, recvFrom)
import Network.Socket.ByteString
import Control.Concurrent
import Data.List.Split(splitOn)
import System.Environment(getArgs)
import System.Random(randomRIO)
import Data.String.Utils(strip)
import Data.Char(toUpper)

data Server = Server {serverSocket :: Socket,
                      connlist :: MVar [Client],
                      serverKey :: KeyChain}
data Client = Client {clientSocket :: Socket,
                      clientKey :: Key} deriving (Show, Eq)

maxBytes :: Int
maxBytes = 8142

port = 19105

delim :: String
delim = " "

omit :: Eq a => [a] -> a -> [a]
omit arr o = [x | x<-arr, x /= o]

startsWith :: (Eq a) => [a] -> [a] -> Bool
startsWith [] [] = True
startsWith _ [] = True
startsWith [] (y:ys) = False
startsWith (x:xs) (y:ys) = x == y && startsWith xs ys

joinWith :: [[a]] -> [a] -> [a]
joinWith [] _ = []
joinWith [arr] _ = arr
joinWith (a:as) sep = a ++ sep ++ (joinWith as sep)

decryptMsg :: Key -> String -> String
decryptMsg k str = decryptStr k [read s :: Integer |
                                 s<-splitOn delim (strip str), s/=""]

sendKey :: Socket -> Key -> IO ()
sendKey sock key = sendAll sock (BC.pack str) where
  str = (show $ keyVal key) ++ ',':(show $ modVal key) ++ "\n"

sendEncrypted :: Key -> Socket -> String -> IO ()
sendEncrypted _ _ "" = return ()
sendEncrypted k sock str = do
  --putStrLn $ "Sending: " ++ (show dat)
  sendAll sock (BC.pack dat) where
  dat = (map show (encryptStr k str) `joinWith` delim) ++ "\n"

sendToClient :: Client -> String -> IO ()
sendToClient client msg = sendEncrypted (clientKey client) (clientSocket client) msg

sendToAll :: [Client] -> String -> IO ()
sendToAll conns dat = mapM_ (\c -> sendToClient c dat) conns

recvString :: Socket -> IO String
recvString sock = do
  bytes <- recv sock maxBytes
  return $ BC.unpack bytes

yesOrNo :: String -> IO Bool
yesOrNo prompt = do
  putStrLn prompt
  resp <- getLine
  let up = map toUpper resp
  if up `elem` ["Y", "YES"] then
    return True
    else
    if up `elem` ["N", "NO"] then
      return False
      else do
      putStrLn $ "Invalid input: " ++ (show resp)
      yesOrNo prompt

randomKey :: (Integer, Integer) -> IO KeyChain
randomKey range = do
  n <- randomRIO range
  m <- randomRIO range
  generateKey (abs n) (abs m)

userGenKey :: IO KeyChain
userGenKey = do
  rand <- yesOrNo "Random key?[Y/N]"
  if rand then randomKey (10, 999) else do
  putStrLn "First number: "
  line1 <- getLine
  putStrLn "Second number: "
  line2 <- getLine
  let [p,q] = [(read s) :: Integer | s<-[line1, line2]]
  generateKey p q

getKeyFromClient :: Socket -> IO Key
getKeyFromClient sock = do
  keystr <- recvString sock
  putStrLn $ "Parsing key from: " ++ (show keystr)
  let (es:cs:rest) = splitOn "," keystr
  let (e, c) = (read es :: Integer, read cs :: Integer)
  let res = Key e c
  print res
  return res

initClient :: String -> IO ()
initClient hostname = do
  mykey <- userGenKey
  putStrLn $ "Key generated: " ++ (show mykey)
  sock <- socket AF_INET Stream defaultProtocol
  setSocketOption sock ReuseAddr 1
  infos <- getAddrInfo (Just defaultHints) (Just hostname) Nothing
  putStr "Connecting..."
  let addr (SockAddrInet _ a) = a
  connect sock $ SockAddrInet port (addr $ addrAddress (head infos))
  putStr "done!\nSending key..."
  sendKey sock (public mykey)
  putStr "done!\nReceiving key..."
  theirs <- getKeyFromClient sock
  putStrLn "done!"
  forkIO $ listenToServer (private mykey) sock
  runClient theirs sock

listenToServer :: Key -> Socket -> IO ()
listenToServer k sock = do
  conn <- isConnected sock
  read <- isReadable sock
  if conn && read then do
    str <- recvString sock
    putStrLn $ "Recieved: " ++ (decryptMsg k str)
    listenToServer k sock
    else return ()

runClient :: Key -> Socket -> IO ()
runClient k sock = do
  line <- getLine
  if line == ":quit" then do
    sendAll sock (BC.pack "quit\n")
    close sock
    else do
    conn <- isConnected sock
    write <- isWritable sock
    if conn && write then do
      sendEncrypted k sock (strip line)
      runClient k sock
      else do
      putStrLn "Connection was lost."
      close sock

initServer :: IO ()
initServer = do
  key <- userGenKey
  putStrLn $ "Your key: " ++ (show key)
  let waiting = 10
  sock <- socket AF_INET Stream 0
  setSocketOption sock ReuseAddr 1
  bind sock (SockAddrInet port iNADDR_ANY)
  listen sock waiting
  putStrLn "Listening..."
  list <- newMVar []
  runServer $ Server sock list key

crackClientKey :: Key -> IO ()
crackClientKey key = putStrLn $ (show private) ++ "<-- Client's private key!"
  where private = crackKey key

runServer :: Server -> IO ()
runServer server = do
  conn <- accept $ serverSocket server
  putStrLn "Connection accepted!"
  let sock = fst conn
  key <- getKeyFromClient sock
  forkIO $ crackClientKey key
  sendKey sock (public $ serverKey server)
  let client = Client sock key
  putStrLn $ "client = " ++ (show client)
  conns <- takeMVar $ connlist server
  putMVar (connlist server) $ client:conns
  forkIO $ readFromClient server client
  runServer server

closeClient :: Server -> Client -> IO ()
closeClient server client = do
  clients <- takeMVar (connlist server) --Do this first to prevent any sends
  putStrLn "Client quit!"
  shutdown (clientSocket client) ShutdownBoth
  close (clientSocket client)
  putMVar (connlist server) $ clients `omit` client
  return ()

readFromClient :: Server -> Client -> IO ()
readFromClient server client = do
  let sock = clientSocket client
  conn <- isConnected sock
  read <- isReadable sock
  if not $ conn && read then closeClient server client else do
    str <- recvString sock
    putStrLn $ "Recieved " ++ (show str) ++ " from client."
    if str == "" || str `startsWith` "quit" then closeClient server client
      else do
      let msg = decryptMsg (private $ serverKey server) str
      putStrLn $ "Decrypted " ++ (show msg)
      clients <- takeMVar $ connlist server
      putStrLn "Relaying..."
      sendToAll (clients `omit` client) (strip msg)
      putMVar (connlist server) clients
      readFromClient server client

main = do
  args <- getArgs
  let len = length args
  if len == 0 then initServer else initClient (head args)
