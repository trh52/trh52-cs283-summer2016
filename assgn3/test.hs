
import RSA

import System.Random

userGenKey :: IO KeyChain
userGenKey = do
  putStrLn "First number: "
  line1 <- getLine
  putStrLn "Second number: "
  line2 <- getLine
  let [p,q] = [(read s) :: Integer | s<-[line1, line2]]
  generateKey p q

testPairs :: KeyChain -> KeyChain -> IO ()
testPairs client server = do
  putStrLn "Testing pair:"
  putStrLn $ "\tClient:\n\t\t" ++ (show client)
  putStrLn $ "\tServer:\n\t\t" ++ (show server)
  let clientSidePair = KeyPair client (public server) MeFirst ThemFirst
  let serverSidePair = KeyPair server (public client) ThemFirst MeFirst
  let steps x = (x, enc, dec) where
        enc = encrypt clientSidePair x
        dec = decrypt serverSidePair enc
  let unscathed x = x == dec where (_, _, dec) = steps x
  let testspace = [0..127]
  let allUnscathed = all unscathed testspace
  putStrLn $ "All unchanged: " ++ (show allUnscathed)
  if allUnscathed then return () else mapM_ print [steps i | i<-testspace]

testUserGen :: IO ()
testUserGen = do
  putStrLn "Client:"
  client <- userGenKey
  putStrLn "Server:"
  server <- userGenKey
  testPairs client server

testGen :: IO ()
testGen = do
  client <- generateKey 234 175
  server <- generateKey 421 332
  testPairs client server

testSpecific :: IO ()
testSpecific = do
  let client = KeyChain {private = Key {keyVal = 1578085, modVal = 11843537},
                         public = Key {keyVal = 10274437, modVal = 11843537}}
  let server = KeyChain {private = Key {keyVal = 159071, modVal = 1754603},
                         public = Key {keyVal = 1648631, modVal = 1754603}}
  testPairs client server

testRandom :: (Integer, Integer) -> IO ()
testRandom range = do
  p <- randomRIO range
  q <- randomRIO range
  r <- randomRIO range
  s <- randomRIO range
  print (p,q,r,s)
  client <- generateKey p q
  server <- generateKey r s
  testPairs client server

verifyKeyChain :: KeyChain -> [Integer] -> Bool
verifyKeyChain chain vals = all id [doubleRun pub priv x == x
                                    && doubleRun priv pub x == x | x<-vals] where
  pub = public chain
  priv = private chain

main :: IO ()
main = do
  --testGen
  --testSpecific
  --testUserGen
  testRandom (10, 99)
