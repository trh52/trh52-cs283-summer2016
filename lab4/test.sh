#!/bin/bash

#echo "Bash version ${BASH_VERSION}"
#echo "$1 iterations:" > $3
rm $3

for i in `seq 1 $1`
do
    #echo $i
    /usr/bin/time -f %e -o $3 --append $2
done
