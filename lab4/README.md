CS283 - Lab 4 - Concurrent Programming
=====

By: Tobias Highfill (trh52)

The best way to run all the code is:

```
make test-all
```

This will compile and run everything and put data into neat little files for you.

#Shared Variable Stuff

The executable will be `sharedVar`
The runtimes will be in `sharedTest.txt`
The results (what the counter was at the end) will be in `sharedRes.txt`

This method was the second fastest but almost always produced horribly incorrect results at high values of `N_GETUP`.

#Mutex Lock Outside For Loop

The executable will be in `mutexOut`
The runtimes will be in `mutexOutTest.txt`

This method was the fastest and always worked.

#Mutex Lock Inside For Loop

The executable will be in `mutexIn`
The runtimes will be in `mutexInTest.txt`

This method was always right but it ran a lot slower than the other two methods