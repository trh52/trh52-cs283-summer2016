
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define N_PEOPLE 100
#define N_GETUP 100000

int wait_for_threads(pthread_t* threads, size_t length){
  for(int i=0; i<length; ++i){
    if(pthread_join(threads[i], NULL) != 0){
      printf("There was an unexpected error with thread %d\n", i);
      return 2;
    }
  }
  return 0;
}

void* person_shared(void* data){
  volatile long* counter = (volatile long*) data;
  for(int i = 0; i < N_GETUP; i++){
    ++(*counter);
  }
  return NULL;
}

typedef struct counter_t{
  long counter;
  pthread_mutex_t mutex;
} counter_t;

void* person_mutex_inside(void* data){
  counter_t* ctr = (counter_t*) data;
  pthread_mutex_t* mutex = &(ctr->mutex);
  for(int i=0; i < N_GETUP; i++){
    if(pthread_mutex_lock(mutex) != 0){
      printf("Error locking mutex!\n");
      return NULL;
    }
    ++(ctr->counter);
    if(pthread_mutex_unlock(mutex) != 0){
      printf("Error unlocking mutex!\n");
      return NULL;
    }
  }
  return NULL;
}

void* person_mutex_outside(void* data){
  counter_t* ctr = (counter_t*) data;
  pthread_mutex_t* mutex = &(ctr->mutex);
  if(pthread_mutex_lock(mutex) != 0){
    printf("Error locking mutex!\n");
    return NULL;
  }
  for(int i=0; i < N_GETUP; i++){
    ++(ctr->counter);
  }
  if(pthread_mutex_unlock(mutex) != 0){
    printf("Error unlocking mutex!\n");
    return NULL;
  }
  return NULL;
}

#ifdef MUTEX_OUTSIDE
#define MUTEX_FUNC person_mutex_outside
#else
#define MUTEX_FUNC person_mutex_inside
#endif

int run_threads(size_t length, void *(*start_routine) (void *), void* data){
  pthread_t threads[length];
  for(int i=0; i<length; ++i){
    if(pthread_create(threads+i,
		      NULL,
		      start_routine,
		      data) != 0){
      printf("Unable to create thread %d\n", i);
      return 1;
    }
  }
  wait_for_threads(threads, length);
  return 0;
}

int run_shared(){
  volatile long result = 0;
  int ret = run_threads(N_PEOPLE, person_shared, (void*) &result);
  printf("%ld\n", result);
  return ret;
}

int run_mutex(){
  counter_t ctr;
  ctr.counter = 0;
  pthread_mutex_init(&(ctr.mutex), NULL);
  int result = run_threads(N_PEOPLE, MUTEX_FUNC, (void*) &ctr);
  pthread_mutex_destroy(&(ctr.mutex));
  printf("%ld\n", ctr.counter);
  return result;
}

int main(){
#ifdef SHARED
  return run_shared();
#else
  return run_mutex();
#endif
}
