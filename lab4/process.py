#!/usr/bin/python

import sys

def getTime(timeStr):
    res = 0
    for s in timeStr.split(':'):
        res *= 60
        res += float(s)
    return res

def process(filename):
    with open(filename, 'r') as f:
        times = [getTime(line) for line in f]
        return sum(times)/len(times)

def main():
    for fname in sys.argv[1:]:
        print "File: %s\tAverage Time: %f s" % (fname, process(fname))

if __name__ == '__main__':
    main()
