#!/usr/bin/python

import socket
import csv
import mrserver, sequential
import sys

def endReadOn(f, endOn):
    for line in f:
        line = line.lstrip()
        #sys.stdout.write(line+'\r')
        if line.startswith(endOn):
            #print "\nGot endmark!"
            return
        else:
            yield line

def mapData(f, endOn):
    return sequential.mapData(endReadOn(f, endOn))

def main():
    if len(sys.argv) < 2:
        print "must give hostname!"
        return
    sock = socket.create_connection((sys.argv[1], mrserver.PORT))
    inFile = sock.makefile('r')
    outFile = sock.makefile('w')
    data = mapData(inFile, mrserver.END_STREAM)
    #print "\nData mapped!"
    inFile.close()
    reduced = sequential.reduceData(data)
    for k in reduced:
        sum_, count = reduced[k]
        outFile.write(k + mrserver.DELIMITER + str(sum_) + ','+str(count)+'\n')
    outFile.write(mrserver.END_STREAM)
    #print "\nData sent!"
    outFile.close()
    sock.shutdown(socket.SHUT_RDWR)
    sock.close()

if __name__ == '__main__':
    main()
