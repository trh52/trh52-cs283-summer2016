#!/usr/bin/python

import sys

def getTime(timeStr):
    res = 0
    for s in timeStr.split(':'):
        res *= 60
        res += float(s)
    return res

def process(filepath):
    dat = None
    with open(filepath, 'r') as f:
        dat = getTime(f.read().strip())
    return dat

def main():
    if len(sys.argv) < 3:
        print "Need at least two files!"
    base = None
    print "Base:"
    for fpath in sys.argv[1:]:
        time = process(fpath)
        sys.stdout.write('File: %s\tTime: %f secs' % (fpath,time))
        if base is None:
            base = time
            print "\nOthers:"
        else:
            perc = 100*time/base
            print " (%f%% base speed)" % perc

if __name__ == '__main__':
    main()
            
