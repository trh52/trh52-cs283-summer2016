#!/bin/bash

/usr/bin/time -f %e -o mrtime-$1.txt ./mrserver.py $1 &

sleep 1

for i in `seq 1 $1`
do
    ./mrclient.py localhost &
done

wait
