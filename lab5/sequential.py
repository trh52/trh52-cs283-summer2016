#!/usr/bin/python

import numpy as np
import csv
import sys

def parseStatus(stat):
    if stat == 'On Time':
        return 0
    return int(stat.split(' ')[0])

def mapData(f, printstuff = False):
    railLines = {}
    reader = csv.DictReader(f)
    for i,row in enumerate(reader):
        if printstuff:
            sys.stdout.write('Line: %d\r' % (i+1))
        key = row['origin']
        status = parseStatus(row['status'])
        if key in railLines:
            railLines[key].append(status)
        else:
            railLines[key] = [status]
    return railLines

def reduceData(railLines):
    return {k:(sum(railLines[k]),len(railLines[k])) for k in railLines}

def main():
    #print "Mapping..."
    railLines = None
    with open('otp.csv', 'r') as f:
        railLines = mapData(f, False)
    #print "\nReducing..."
    reduced = reduceData(railLines)
    for k in reduced:
        sum_, count = reduced[k]
        print k + ": " + str(float(sum_)/count) + ' mins'

if __name__ == '__main__':
    main()
