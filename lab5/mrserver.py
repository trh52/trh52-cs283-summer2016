#!/usr/bin/python

import numpy as np
import socket
import sys
from threading import Thread, Lock

END_STREAM = "END STREAM\n"
DELIMITER = "\t"
PORT = 58732

def initServer(port = PORT, backlog = 5):
    sock = socket.socket()
    sock.bind(("", port))
    sock.listen(backlog)
    return sock

def waitForConns(sock, connCount):
    conns = []
    for i in xrange(connCount):
        #print "listening..."
        conn, _ = sock.accept()
        conns.append(conn)
        #print "accepted!"
    return conns

def transmit(outFiles, filepath='otp.csv'):
    connCount = len(outFiles)
    with open(filepath, 'r') as f:
        header = f.readline()
        for ofile in outFiles:
            ofile.write(header)
        #print "Headers sent"
        for i,line in enumerate(f):
            outFiles[i % connCount].write(line.rstrip()+'\n')
            #sys.stdout.write('Sent line %d\r' % i)
        #print "\nSending endmarks"
        for ofile in outFiles:
            ofile.write(END_STREAM)
    for f in outFiles:
        f.close()

def main():
    if len(sys.argv) < 2:
        print "You must pass the number of connections!"
        return
    server = initServer()
    connCount = int(sys.argv[1])
    conns = waitForConns(server, connCount)
    outFiles = [sock.makefile('w') for sock in conns]
    inFiles = [sock.makefile('r') for sock in conns]
    threads = []
    mastermap = {}
    maplock = Lock()
    for inFile in inFiles:
        def tfunc(ifile = inFile, mmap=mastermap, lock=maplock):
            for line in ifile:
                if line.startswith(END_STREAM):
                    break
                else:
                    key, val = line.split(DELIMITER)
                    sum_, count = [int(s.strip()) for s in val.split(',')]
                    lock.acquire()
                    c_sum, c_count = mmap.get(key, (0,0))
                    mmap[key] = (sum_+c_sum, count+c_count)
                    lock.release()
            ifile.close()
        thread = Thread(target = tfunc)
        thread.start()
        threads.append(thread)
    #print "Transmitting..."
    transmit(outFiles)
    #print "Waiting for my slaves to finish..."
    openstreams = connCount
    for thread in threads:
        #print '%d open streams' % openstreams
        if thread.isAlive():
            thread.join()
        openstreams -= 1
    #print 'Finished!'
    for sock in conns + [server]:
        sock.shutdown(socket.SHUT_RDWR)
        sock.close()
    for key in mastermap:
        sum_, count = mastermap[key]
        print '%s: %f mins' % (key, float(sum_)/count)
    print "Done!"
    

if __name__ == '__main__':
    main()
