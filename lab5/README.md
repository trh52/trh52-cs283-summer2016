CS 283 Lab 5 - MapReduce
=====

By: Tobias Highfill (trh52)

#Running the tests

Simply:

```
make clean #If data already exists, erase it
make
```

This will print out all the times and percent changes for the tests

#Results

The MapReduce version runs in about half the time when you use 8 nodes and that was typically the best performance I got. I think the 16 was slower due to all the overhead encumberences outweighing the benefits of the lighter individual workload. Oddly, 2 nodes works in about the same time as the sequential implementation and is usually a little bit slower. This is also probably due to overhead.

#A note on the data

I wasn't really sure how to identify the line the train was on because you only get the source of the train so I just used that